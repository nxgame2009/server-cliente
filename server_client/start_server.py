from server.main import run_server
from _thread import *
import sys


start_new_thread(run_server,())

while True:
    in_key = input('>: ')
    if in_key == '/quit':
        print('Fechando o servidor')
        sys.exit()
    print(in_key)
