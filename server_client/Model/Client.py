class Client():
    def __init__(self, ip, port, data):
        self.ip = ip
        self.port = port
        self.__username = data['data']
        self.__header = data['header']

    def username(self):
        return self.__username.decode('utf-8')

    def header(self):
        return self.__header.decode('utf-8')


    def busername(self):
        print(self.__header + self.__username)
        return self.__header + self.__username

    def __repr__(self):
        return f"{self.ip}:{self.port} username:{self.username()}"
