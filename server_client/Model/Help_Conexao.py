import sys

HEADER_LENGTH = 10
def recv(client_socket, closeConexao=True):
    try:
        message_header = client_socket.recv(HEADER_LENGTH)
        if not len(message_header):
            if closeConexao:
                sys.exit()
            return False
        message_length = int(message_header.decode('utf-8').strip())

        tmsg = client_socket.recv(message_length)
        return {
                    "header": message_header,
                    "msg": tmsg.decode('utf-8'),
                    "binary": f"{len(tmsg):<{HEADER_LENGTH}}{tmsg}"
                    .encode('utf-8')
                }
    except Exception as e:
        print(e)
        return False
