import socket
import select
import sys
import pickle
from datetime import datetime
from Model.Client import Client
from configs.config import Config


c = Config('server')

HEADER_LENGTH = 10
host = c.config('server','ip')
port = c.config('server','port')


server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
server_socket.bind((host,int(port)))
server_socket.listen()

socket_list = [server_socket]

clients = {}
def sprint(string):
    now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print(f'{now} {string}\n>: ', end='')


def receive_message(client_socket):
    try:
        message_header = client_socket.recv(HEADER_LENGTH)
        if not len(message_header):
            return False
        message_length = int(message_header.decode('utf-8').strip())


        return {
                    "header": message_header,
                    "data":client_socket.recv(message_length)
                }
    except:
        return False

print("Start server")

def run_server():
    while True:
        read_sockets, _ , exception_socktes = select.select(socket_list,[],socket_list)
        for notified_socket in read_sockets:
            if notified_socket == server_socket:
                client_socket, client_address = server_socket.accept()

                user = receive_message(client_socket)

                if user is False:
                    continue
                socket_list.append(client_socket)
                c = Client(client_address[0], client_address[1], user)
                clients[client_socket] = c
                sprint(f"Accept new connection from {c}")

            else:
                message = receive_message(notified_socket)
                if message is False:
                    sprint(f"Closed connection from {clients[notified_socket].username()}")
                    socket_list.remove(notified_socket)
                    del clients[notified_socket]
                    continue

                user = clients[notified_socket]
                sprint(f"Received message from {user.username()}: {message['data'].decode('utf-8')}")

                for client_socket in clients:
                    print(client_socket)
                    if client_socket != notified_socket:
                        client_socket.send(user.busername() + message['header'] + message['data'])

        for notified_socket in exception_socktes:
            socket_list.remove(notified_socket)
            del clients[notified_socket]
