import socket
import select
import errno
import sys
from _thread import *
from configs.config import Config

HEADER_LENGTH = 10
c = Config('server')
host = c.config('server','ip')
port = c.config('server','port')


my_username = input('UserName: ')

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((host, int(port)))
client_socket.setblocking(False)


def send(msg):
    msg_header = f"{len(msg):<{HEADER_LENGTH}}".encode('utf-8')
    client_socket.send(msg_header + msg)


def recv(closeClient= False):
    tmp_header = client_socket.recv(HEADER_LENGTH)
    if not len(tmp_header) and closeClient:
        print("conexao fechada pelo servido")
        sys.exit()

    tmp_length = int(tmp_header.decode('utf-8').strip())
    return client_socket.recv(tmp_length).decode('utf-8')


send(my_username.encode('utf-8'))


def run_listen():
    while True:
        try:
            while True:
                username = recv(True)
                message = recv()

                print(f"{username} > {message} ")
        except IOError as e:
            if e.errno != errno.EAGAIN and e.errno != errno.EWOULDBLOCK:
                print('Erro de leitura', str(e))
                sys.exit()
            continue
        except Exception as e:
            print('Erros generico:', str(e) )
            sys.exit()


def comande():
    while True:

        message = input(f"{my_username} > ")

        if message:
            send(message.encode('utf-8'))


start_new_thread(comande,())
run_listen()
